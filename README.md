# React Main Componet 

This project is used to test modular components

## Install

```
git clone https://gitlab.com/zaplib/react-main-component.git
cd react-main-component
npm install
```

## Dev Mode

```
npm run start
```

## Prod Mode

```
npm run build
```

## ENV Version

* **Node.js: v9.7.1**
* **npm: 5.6.0**


## Package Version

* **react: 16**
* **redux: 4** 
* **react-router-dom: 4** 
* **connected-react-router: 6** (棄用react-router-redux)
* **postcss-loader: 3**

## Dev Package Version

* **babel: 7**
* **webpack: 4**
