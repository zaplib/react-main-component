import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
//import example from './example'
import {Reducer} from 'sidecomponent'

export default (history) => combineReducers({
    ...Reducer,
    router: connectRouter(history),
})